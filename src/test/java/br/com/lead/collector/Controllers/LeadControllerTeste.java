package br.com.lead.collector.Controllers;


import br.com.lead.collector.controllers.LeadController;
import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.annotation.Before;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(controllers = LeadController.class)
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
//        lead.setId(1);
        lead.setNome("Vinicius");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("vinicius@gmail.com");

        produto = new Produto();
        produto.setDescricao("Café do boum");
  //      produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(30.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

//    @Test
//    public void testarBuscarPorId() throws Exception {
//       Lead leadObjeto = new Lead();
//
//        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadObjeto);
//
//
//
//        mockMvc.perform(MockMvcRequestBuilders.post("/leads/{id}") //o perform é o cara que manda requisição falsa
//                .contentType(MediaType.APPLICATION_JSON)  //tipo de formato
//                .content(leadObjeto))// o que estamos enviando
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$.lead", CoreMatchers.equalTo(lead))) //validar se esta voltando o valor certo comparando com o jsonpath...no caso é o ID
//
//        ;
//    }

    @Test
    public void testarRegistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            return lead;
        });//simulando o id do bnanco de dados voltando o id

        //converter o lead pro json
        ObjectMapper mapper = new ObjectMapper(); //converte objeto pra json ou vice versa

        String jsonDeLead = mapper.writeValueAsString(lead); //ele converte em json usando os geter e seters

        //em teste de controle nao usa assepts. A gente usa o proprio mockito mvc

        mockMvc.perform(MockMvcRequestBuilders.post("/leads") //o perform é o cara que manda requisição falsa
               .contentType(MediaType.APPLICATION_JSON)  //tipo de formato
               .content(jsonDeLead))// o que estamos enviando
               .andExpect(MockMvcResultMatchers.status().isCreated())  //validar se foi criado
               .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1))) //validar se esta voltando o valor certo comparando com o jsonpath...no caso é o ID
               .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo(LocalDate.now().toString())))   //qui nessa linha ele esta validando o campo de data... a cada campo, vou colocando mais uma linha
        ;
    }
}
