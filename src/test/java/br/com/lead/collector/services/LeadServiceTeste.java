package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    @MockBean
    private LeadRepository leadRepository;
    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setId(1);
        lead.setNome("Vinicius");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("vinicius@gmail.com");

        produto = new Produto();
        produto.setDescricao("Café do boum");
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(30.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscarPorTodosOsLeads(){
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarSalvarLead(){
        Mockito.when(produtoService.buscarPorTodosOsId(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Jeff");
        leadTeste.setProdutos(produtos);
        leadTeste.setEmail("jeff@mt.com.br");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);

        Mockito.when(leadRepository.save(leadTeste)).thenReturn(leadTeste);

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));
    }

    @Test
    public void testarBuscaPorId(){
        Lead lead = new Lead();
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);
        Lead lead1 = leadService.buscarPorId(lead.getId());

        Assertions.assertEquals(lead, lead1);
    }

    @Test
    public void testarAtualizarLead(){
        Lead lead = new Lead();
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(Mockito.anyBoolean());
        lead.setId(Mockito.anyInt());



        Lead lead1 = leadService.atualizarLead(Mockito.anyInt(), lead);


        Assertions.assertEquals(lead, lead1);


    }
    @Test
    public void testarDeletarLead(){
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(true);
        leadService.deletarLead(5);

        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(Mockito.anyInt());

    }



}





