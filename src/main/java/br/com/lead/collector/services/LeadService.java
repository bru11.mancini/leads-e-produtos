package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired  //como é injeção de dependencia, tem que usar o autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoService produtoService;

    public Lead salvarLead(Lead lead){
        List<Integer> idProdutos = new ArrayList<>();

        for (Produto produto : lead.getProdutos()) {
            int id = produto.getId();
            idProdutos.add(id);
        }

        List<Produto> produtos = produtoService.buscarPorTodosOsId(idProdutos);


        lead.setProdutos(produtos);
        LocalDate data = LocalDate.now();
        lead.setData(data);
        Lead leadObjeto = leadRepository.save(lead);

        return leadObjeto;
    }

    public Iterable<Lead> buscarTodos(){  //O Iterable é mais rapido do que a lista... pouca, mas tem diferença
        Iterable<Lead> leads = leadRepository.findAll();
        return leads;

    }

    public  Iterable<Lead> buscarTodosPorTipoLead(TipoLeadEnum leadEnum){
        Iterable<Lead> leads = leadRepository.findAllByTipoLead(leadEnum);
        return leads;
    }

    public Lead buscarPorId(int id){
        Optional<Lead> optionalLead = leadRepository.findById(id); //Com o optional ele evita que dê logo de primeira um nullpointexpecion
        if (optionalLead.isPresent()){   //se tiver coisa no optional
            return optionalLead.get(); //ele pega o que tinha no optional
        }
        throw new RuntimeException("O lead não foi encontrado");
    }

    public Lead atualizarLead(int id, Lead lead){
//        buscarPorId(id);

        if (leadRepository.existsById(id)){
            lead.setId(id);
            Lead leadObjeto = salvarLead(lead);

            return leadObjeto;
        }
        throw new RuntimeException("O lead não foi encontrado");

    }

    public void deletarLead(int id) {
        if (leadRepository.existsById(id)) {
            leadRepository.deleteById(id);

        } else {
            throw new RuntimeException("O lead não foi encontrado");
        }
    }

}
