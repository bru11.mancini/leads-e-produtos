package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired  //como é injeção de dependencia, tem que usar o autowired
    private ProdutoRepository produtoRepository;


    public Produto salvarProduto(Produto produto){
        Produto produtoObjeto = produtoRepository.save(produto);

        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodos(){  //O Iterable é mais rapido do que a lista... pouca, mas tem diferença
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;

    }

    public  Iterable<Produto> buscarTodosPorNomeProduto(String nomeProduto){
        Iterable<Produto> produtos = produtoRepository.findAllByNomeContaining(nomeProduto);
        return produtos;
    }

    public Produto buscarPorId(int id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id); //Com o optional ele evita que dê logo de primeira um nullpointexpecion
        if (optionalProduto.isPresent()){   //se tiver coisa no optional
            return optionalProduto.get(); //ele pega o que tinha no optional
        }
        throw new RuntimeException("O produto não foi encontrado na consulta");
    }

    public Produto atualizarProduto(int id, Produto produto){
        if (produtoRepository.existsById(id)){
            produto.setId(id);
            Produto produtoObjeto = salvarProduto(produto);

            return produtoObjeto;
        }
        throw new RuntimeException("O produto não foi encontrado para ser atualizado");

    }

    public Produto atualizarNomeProduto (int id, String nomeProduto) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent()) {
            Produto produto = optionalProduto.get();
            produto.setId(id);
            produto.setNome(nomeProduto);
            return produto = salvarProduto(produto);

            //Produto produto1 = salvarProduto(produto);
            //return produto1;

        }
        throw new RuntimeException("O produto não foi encontrado para ter o nome atualizado");
    }


    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);

        } else {
            throw new RuntimeException("O produto não foi encontrado para ser deletado");
        }
    }

    public Iterable<Produto> buscarTodosOsProdutos(){
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public List<Produto> buscarPorTodosOsId(List<Integer> id){
        Iterable<Produto> produto = produtoRepository.findAllById(id);
        return (List) produto;
    }


}
