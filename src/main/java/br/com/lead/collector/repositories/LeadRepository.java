package br.com.lead.collector.repositories;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> { //1º paramretro é a model e o 2º parametro é a chave primaria que
    // tem que ser o wrapper dele que é o Integer ---- int -> Integer



    //PARA FAZER UM METODO NOVO, POR SER UMA INTERFACE, A GENTE NAO COLOCA O TIPO DE RETORNO
    //isso que criei na linha 17, o jpa faz automaticamente uma query com o campo Email

    //Se precisar fazer uma query especifica, usar o @Query
    //@Query("Select * from lead WHERE lead.email == ?")   AI quando eu chamar o metodo 17, ele executa a query
    Iterable<Lead> findAllByTipoLead(TipoLeadEnum leadEnum);




}
