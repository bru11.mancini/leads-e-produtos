package br.com.lead.collector.controllers;


import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/produtos")  //O endpoint relacionado à model é ter o nome da model no plural
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;


    @PostMapping    //O post significa que quer cadastrar algo no banco de dados
    public Produto registrarProduto(@RequestBody Produto produto){
        return produtoService.salvarProduto(produto);
    }

    @GetMapping
    public Iterable<Produto> exibirTodos (@RequestParam(name = "nomeDoProduto", required = false)String nomeProduto) { //Aqui nesse caso
        // ele esperar vir uma variavel pela url e ai ele entrega pro nomeDoProduto. Se vier
        //o required = false serve para nao ser obrigatório o campo. Entao nesse caso, se preenchido, procuramos por ele,
        //Se nao vier preenchido ele assume null e cai na linha 31 após o IF da linha 27
        if (nomeProduto != null){
            Iterable<Produto> produtos = produtoService.buscarTodosPorNomeProduto(nomeProduto);
            return produtos;
        }
        Iterable<Produto> produtos = produtoService.buscarTodos();
        return produtos;
    }

    @GetMapping("/{id}")
    public Produto buscarPorId(@PathVariable (name = "id") int id) {
        try {
            Produto produto = produtoService.buscarPorId(id);
            return produto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable (name = "id") int id, @RequestBody Produto produto) { //No requestbody vao os dados que vão atualizar
        try {
            Produto produtoObjeto = produtoService.atualizarProduto(id, produto);
            return produtoObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/atualiza/{id}")  //trocar por path e manter o mesmo endpoint ("/{id}")
    public Produto atualizarNomeDoProduto(@PathVariable (name = "id") int id, @RequestBody  String nomeProduto){
    //public Produto atualizarNomeDoProduto(@PathVariable (name = "id") int id, @RequestBody  Produto nomeProduto){
    //    System.out.println("Nome" + nomeProduto.getNome());
    //    System.out.println("Preço " + nomeProduto.getPreco());
    //    return nomeProduto;
    //Esses comentarios acima foi para o professor ensinar que quando eu quero fazer uma atualização dessa, o certo é:
    //Usar o PathMapping, deixar entrar o objeto inteiro (mesmo que venha null ou 0 os valores não preenchidos) e então
    //Eu comparar cada campo recebido com o campo na entidade preenchida e ver o que está diferente e o que está diferente, atualizar.
        try {
            Produto produto = produtoService.atualizarNomeProduto(id, nomeProduto);
            return produto;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarProduto(@PathVariable (name = "id") int id) {
        try {
            produtoService.deletarProduto(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
