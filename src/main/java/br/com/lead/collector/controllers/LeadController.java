package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.accessibility.AccessibleRelation;
import javax.validation.Valid;

@RestController
@RequestMapping("/leads")  //O endpoint relacionado à model é ter o nome da model no plural
public class LeadController {

    @Autowired
    private LeadService leadService;


    @PostMapping    //O post significa que quer cadastrar algo no banco de dados
    @ResponseStatus(HttpStatus.CREATED)
    public Lead registrarLead(@RequestBody @Valid Lead lead){ //inclui o valid para validar o que colocamos de validação na model
        return leadService.salvarLead(lead);

    }

    @GetMapping
    public Iterable<Lead> exibirTodos (@RequestParam(name = "tipoDeLead", required = false)TipoLeadEnum tipoLeadEnum) { //Aqui nesse caso
        // ele esperar vir uma varuaivel pela url e ai ele entrega pro tipoLeadEnum. Se vier
        //o required = false serve para nao ser obrigatorio o campo. Entao nesse caso, se preenchido, procuramos por ele,
        //Se nao vier preenchido ele assume null e cai na linha 31 após o IF da linha 27
        if (tipoLeadEnum != null){
            Iterable<Lead> leads = leadService.buscarTodosPorTipoLead(tipoLeadEnum);
            return leads;
        }
        Iterable<Lead> leads = leadService.buscarTodos();
        return leads;
    }

    @GetMapping("/{id}")
    public Lead buscarPorId(@PathVariable (name = "id") int id) {
        try {
            Lead lead = leadService.buscarPorId(id);
            return lead;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable (name = "id") int id, @RequestBody Lead lead) {
        try {
            Lead leadObjeto = leadService.atualizarLead(id, lead);
            return leadObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletarLead(@PathVariable (name = "id") int id) {
        try {
            leadService.deletarLead(id);
            return ResponseEntity.status(204).body("");
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
