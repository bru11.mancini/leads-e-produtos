package br.com.lead.collector.configurations;

import br.com.lead.collector.security.FiltroDeAutenticacao;
import br.com.lead.collector.security.JWTUtil;
import br.com.lead.collector.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@Configuration      //Marcar como classe de configuração
@EnableWebSecurity  //Serve pra ele pegar a minha classe de segurança e nao a padrao do spring security
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter { //esse adapter vai trazer os métodos que iremos reescrever

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    LoginUsuarioService loginUsuarioService;

    //criamos uma constante com os valores a serem liberados... sempre que for CONSTANTE, ESCREVER EM MAIUSCULO E SNAKE CASE
    private static final String[] PUBLIC_MATCHERS_GET = {
        "/leads",
        "/leads/*",   //<- esse aqui eu estou liberando todos as outras urls
        "/produtos",
        "/login",
        "/usuario"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
        "/leads",
        "/produtos",
        "/usuario",
        "/login"
    };



    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();  //para o post eu tive que desabilitar
        http.cors();

        //super.configure(http); // como eu nao quero o metodo original, eu tiro o super
        http.authorizeRequests() //para as requisições
            .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll() //onde ocorrer o match, comparar e encontrar igual esse metodo get no /leads, dou acesso
            .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
            .anyRequest().authenticated();                                 //para os demais, eu preciso de autenticação

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);// pq nossa api é stateless, nao usamos sessão no nosso sistema, ajuda a liberar consumo de memoria tbm

        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));
        }

        //precisamos liberar o CORS
    @Bean
    protected CorsConfigurationSource configuracaoDeCors(){
            final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
            cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues()); //libera para todas urls do sistema
            return cors;
        }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth); //nao quero esse
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bCryptPasswordEncoder());
    }
}
