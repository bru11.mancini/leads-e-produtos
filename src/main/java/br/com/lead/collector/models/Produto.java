package br.com.lead.collector.models;



import javax.persistence.*;
import java.awt.*;
import java.time.LocalDate;

//Criei um construtor vazio e os getters e setters (por padrão java bean)

@Entity   //Serve pra falar que isso vai ser uma entidade, mapeando a model como uma entidade que deve ser gravada no
// banco de dados, e por sua vez ela vai representar uma tabela no banco de dados
//@Table(name = "Produtos")
public class Produto {



    @Id  //O Id tem que ser unico, sequencial e gerado automaticamente... essas coisas sao feitas no generated
    @GeneratedValue(strategy = GenerationType.IDENTITY)  //Ele vai aplicar na criação dessa tabela, o ID como um valor automatico no banco de dados
    private int id;

    private String  nome;
    private String  descricao;
    private double  preco;


    public Produto(){
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }


}
