package br.com.lead.collector.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@JsonIgnoreProperties(value = "senha", allowSetters = true)  //isso aqui ja nao envia a senha de volta pro usuario
public class Usuario {

    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;


    private String nome;
    @CPF(message = "Cpf inválido")
    @NotNull
    private String cpf;

    @Email(message = "Email invalido")
    @NotNull
    @Column(unique = true)
    private String email;

    @NotNull
    private String senha;

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
