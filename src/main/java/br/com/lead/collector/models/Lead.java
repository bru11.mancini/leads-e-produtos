package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.awt.*;  //Esse danado tava importando esse ao inves do util.List
import java.util.List;
import java.time.LocalDate;

//Criei um construtor vazio e os getters e setters (por padrão java bean)

@Entity   //Serve pra falar que isso vai ser uma entidade, mapeando a model como uma entidade que deve ser gravada no
// banco de dados, e por sua vez ela vai representar uma tabela no banco de dados
//@Table(name = "Leads")
@JsonIgnoreProperties(value = {"data"}, allowGetters =true) //esse aqui é o mesmo que o da linha 37
public class Lead {

  //  @Column(name = "nome_completo")



    @Id  //O Id tem que ser unico, sequencial e gerado automaticamente... essas coisas sao feitas no generated
    @GeneratedValue(strategy = GenerationType.IDENTITY)  //Ele vai aplicar na criação dessa tabela, o ID como um valor automatico no banco de dados
    private int id;

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 a 100 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome precisa estar preenhcido")
    private String  nome;

    @Email(message = "O formato do email é inválido")
    private String  email;

    //@Null(message = "O campo data não é para ser preenchido pelo usuário", payload = )
    //@JsonIgnoreProperties   //pode ser por aqui campo a campo ou la em cima
    private LocalDate data;

    //Se tiver o enum no banco de dados
    //@Enumerated(EnumType.ORDINAL)
    //private TipoLeadEnum tipoLead;

    private TipoLeadEnum tipoLead;

    @ManyToMany(cascade = CascadeType.ALL)  //serve para fazer o relacionamento com outra entidade
    private List<Produto> produtos;

    //Se fosse em tabelas, usariamos o @JoinTable(name="produto_lead", JoinColumn = @JoinColumn" ", etc


    // Se fosse varios leads para um produto, usariamos como abaixo.
    // @ManyToOne Varios leads para um produto
    //private Produto produto;

    public Lead(){
    }



    public int getId() {  //aqui eu tive que colocar o get para voltar no retorno depois que salvou
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
