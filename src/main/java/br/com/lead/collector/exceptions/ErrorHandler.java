package br.com.lead.collector.exceptions;

import br.com.lead.collector.exceptions.errors.MensagemDeErro;
import br.com.lead.collector.exceptions.errors.ObjetoDeErro;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

//Ela precisa ser um controlar do nosso sistema. Ele é controlador específico.
//Responsável por capturar as exceções do sistema e retornar no formato esperado, pode ser json, mvc, etc


@ControllerAdvice
public class ErrorHandler {

    @Value("${mensagem.padrao.de.validacao}")
    private String mensagemPadrao;

    //criamos metodo hashmap com 2 strings,

    //temos que colocar essa anotação para capturarmos uma exceção especifica
    @ExceptionHandler(MethodArgumentNotValidException.class)  //Nesse caso quando da um erro naquelas validações que fizemos do email(por exemplo), ele vem pra ca
    //O .class significa que quero capturar tudo que gerar nessa classe "MethodArgumentNotValidExceptiom)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody  //quer dizer que no corpo da resposta, vai vir o retorno desse metodo
    public MensagemDeErro manipulacaodeErrosDeValidacao(MethodArgumentNotValidException exception){ //ele captura tudo que veio no MethodArgumentNotValidException exception
        //nesse caso o metodo vai sempre retornar esse tipo de erro

        HashMap<String, ObjetoDeErro> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult(); //esse é o objeto que guarda o resultado de todas validações que o sistema fez

        //Lista de erros
        List<FieldError> fieldErros = resultado.getFieldErrors();

        for(FieldError erro : fieldErros){
            erros.put(erro.getField(), new ObjetoDeErro(erro.getDefaultMessage(), erro.getRejectedValue().toString()));


    }

        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
               mensagemPadrao, erros);

        return mensagemDeErro;
    }

}
