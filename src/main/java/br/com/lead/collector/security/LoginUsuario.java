package br.com.lead.collector.security;

//o login de usuario tbm poderia ser uma Model ou um DTO

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class LoginUsuario implements UserDetails {

    private  int id;
    private String email;
    private String senha;

    public LoginUsuario() {
    }

    public LoginUsuario(int id, String email, String senha) {
        this.id = id;
        this.email = email;
        this.senha = senha;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        //return null;
        return senha;
    }

    @Override
    public String getUsername() {
        //return null;
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {    //aqui é pra se quiser expirar uma conta por falta de uso
        //return false;  //caso esteja expirado
        return true; //caso não queira expirar
    }

    @Override
    public boolean isAccountNonLocked() {
        //return false;  //se a conta esta bloqueada
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //return false; //qujer dizer que nao foram expiradas
        return true;
    }

    @Override
    public boolean isEnabled() {
        //return false; // se a conta esta ativa
        return true;
    }
}
