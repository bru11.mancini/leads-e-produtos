package br.com.lead.collector.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.util.Date;

@Component //assim consegue utilizar ele como se fosse um componente auxiliar das outras classes
public class JWTUtil {

    @Value("${jwt.segredo}")
    private String segredo; //palavra chave ou conjunto de caracteres para a criptografia do token

    @Value("${jwt.expiracao}")
    private Long expiracao;


    public String gerarToken(String username){ //receber o email do usuario
        String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis()+expiracao))//aqui estamos somando o atual milisegundo com o que parametrizamos de expiracao
                .signWith(SignatureAlgorithm.HS512, segredo.getBytes())
                .compact();
        return token;
    }



}
