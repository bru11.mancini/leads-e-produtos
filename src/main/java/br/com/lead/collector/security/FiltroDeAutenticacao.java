package br.com.lead.collector.security;

import br.com.lead.collector.models.DTO.CredenciaisDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class FiltroDeAutenticacao extends UsernamePasswordAuthenticationFilter {

    //vamos precisar desses 2 atributos pra trabalhar com esse filtro de autenticação
    private AuthenticationManager authenticationManager;
    private JWTUtil jwtUtil;

    public FiltroDeAutenticacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @Override //abaixo esta o metodo da autenticacao
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException { //receber via http as credenciais do usuario
        //return super.attemptAuthentication(request, response);  sobrescrever ele pq nao quero esse

        ObjectMapper objectMapper = new ObjectMapper();  //esse é um conversor de objeto pra json ou vice versa
        try {
            CredenciaisDTO credenciais = objectMapper.readValue(request.getInputStream(), CredenciaisDTO.class); //elas virao por http request, entao preciso converter o json que vou receber em uma dto

            UsernamePasswordAuthenticationToken usernameToken =
                    new UsernamePasswordAuthenticationToken(credenciais.getEmail(), credenciais.getSenha(), new ArrayList<>()); //passaremos os parametros do dto para o authenticationtoken

            Authentication authentication = authenticationManager.authenticate(usernameToken);   //depois do username em maos, faço a autenticação... esse token que acabei de receber esta autenticado
            return authentication;


        } catch (IOException e) {

            throw new RuntimeException(e.getMessage());
        }
    }



        @Override
        protected void successfulAuthentication (HttpServletRequest request, HttpServletResponse response, FilterChain
        chain,
                Authentication authResult) throws IOException, ServletException {

            String username = ((LoginUsuario) authResult.getPrincipal()).getUsername();
            String token = jwtUtil.gerarToken(username);

            response.addHeader("Authorization", "Bearer" + token); //ideal é responder o token pelo header
        }
    }



